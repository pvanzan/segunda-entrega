title_data = [{
    "id":"1",
    "name":"Copa Libertadores",
    "year":"2022",
    "title_photo":
    "https://pbs.twimg.com/media/FgROeFKWAAEQeB1?format=jpg&name=900x900"
}, {
    "id":"2",
    "name":"Copa do Brasil",
    "year":
    "2022",
    "title_photo":
    "https://img.r7.com/images/flamengo-campeao-flamengo-flamengo-copa-do-brasil-20102022010858376?dimensions=1280x853"
}, {
    "id":"3",
    "name":"Brasileiro",
    "year": 
    "2020",
    "title_photo":
    "https://s2.glbimg.com/60KoafIsgJd20aVAIJvvxE9zZP8=/1200x/smart/filters:cover():strip_icc()/i.s3.glbimg.com/v1/AUTH_bc8228b6673f488aa253bbcb03c80ec5/internal_photos/bs/2021/L/B/1NazdpTWKu4hYrMirP8g/rib0967.jpg"
}, {
    "id":"4",
    "name":"Copa Libertadores",
    "year":"2019",
    "title_photo":
    "https://f.i.uol.com.br/fotografia/2019/11/23/15745553545dd9ceda2a6ed_1574555354_3x2_rt.jpg"
}, {
    "id":"5",
    "name":"Brasileiro",
    "year": 
    "2019",
    "title_photo":
    "https://extra.globo.com/incoming/24105618-a06-89d/w976h550-PROP/85909589_es-rio-de-janeiro-rj-2711201935%C2%AA-rodadacampeonato-brasileiro-2019-flamengo.jpg"
}, {
    "id":"6",
    "name":"Copa do Brasil",
    "year": 
    "2013",
    "title_photo":
    "http://s2.glbimg.com/srqnUFuGAVS39aDY4L2G1mBnoes=/620x470/s.glbimg.com/es/ge/f/original/2013/11/28/flamengo_campeao3_andredurao_95.jpg"
}, {
    "id":"7",
    "name":"Brasileiro",
    "year": 
    "2009",
    "title_photo":
    "https://i.glbimg.com/og/ig/infoglobo1/f/original/2019/12/05/36093669_es_06-12-2009_rio_de_janeiro_rj_final_do_campeonato_brasileiro_2009_flamengo_x_gremio_no_ma.jpg"
}]
