from django.urls import path

from . import views

app_name = 'titles'
urlpatterns = [
    path('', views.TitleListView.as_view(), name='index'),
    path('search/', views.search_titles, name='search'),
    path('create/', views.create_title, name='create'),
    path('<int:title_id>/', views.detail_title, name='detail'),
    path('update/<int:title_id>/', views.update_title, name='update'),
    path('delete/<int:title_id>/', views.delete_title, name='delete'),
    path('<int:title_id>/review/', views.create_review, name='review'),
    path('lists/', views.ListListView.as_view(), name='lists'),
    path('lists/create', views.ListCreateView.as_view(), name='create-list'),
]