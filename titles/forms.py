from django import forms
from django.forms import ModelForm
from .models import Title, Review

class TitleForm(ModelForm):
    class Meta:
        model = Title
        fields = [
            'name',
            'year',
            'title_photo',]
        labels = {
            'name': 'Título',
            'year': 'Ano da conquista',
            'title_photo': 'Foto do título',}

class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
        }
