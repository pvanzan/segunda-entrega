from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .temp_data import title_data
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .models import Title, Review, List
from django.views import generic
from .forms import TitleForm, ReviewForm

def list_titles(request):
    title_list = Title.objects.all()
    context = {'title_list': title_list}
    return render(request, 'titles/index.html', context)

def detail_title(request, title_id):
    title = get_object_or_404(Title,pk=title_id)
    context = {'title': title}
    return render(request, 'titles/detail.html', context)

def search_titles(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        title_list = Title.objects.filter(name__icontains=search_term)
        context = {"title_list": title_list}
    return render(request, 'titles/search.html', context)

def create_title(request):
    form = TitleForm(request.POST)
    if request.method == 'POST':
        title_name = form.cleaned_data['name']
        title_year = form.cleaned_data['year']
        title_title_photo = form.cleaned_data['title_photo']
        title = Title(name = title_name, year = title_year, title_photo = title_title_photo)
        title.save()
        return HttpResponseRedirect(
            reverse('titles:detail', args=(title.id, )))
    else:
        form = TitleForm()
        context = {'form': form}
        return render(request, 'titles/create.html', context)

def update_title(request, title_id):
    title = get_object_or_404(Title, pk=title_id)

    if request.method == "POST":
        form = TitleForm(request.POST)
        if form.is_valid():
            title.name = request.POST['name']
            title.year = request.POST['year']
            title_title_photo = request.POST['title_photo']
            title.save()
            return HttpResponseRedirect(
                reverse('titles:detail', args=(title.id, )))
    else:
        form = TitleForm(
            initial={
                'name': title.name,
                'release_year': title.release_year,
                'poster_url': title.poster_url
            })

    context = {'title': title, 'form': form}
    return render(request, 'titles/update.html', context)


def delete_title(request, title_id):
    title = get_object_or_404(Title, pk=title_id)

    if request.method == "POST":
        title.delete()
        return HttpResponseRedirect(reverse('titles:index'))

    context = {'title': title}
    return render(request, 'titles/delete.html', context)

class TitleListView(generic.ListView):
    model = Title
    template_name = 'titles/index.html'

def create_review(request, title_id):
    title = get_object_or_404(Title, pk=title_id)
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review_author = form.cleaned_data['author']
            review_text = form.cleaned_data['text']
            review = Review(author=review_author,
                            text=review_text,
                            title=title)
            review.save()
            return HttpResponseRedirect(
                reverse('titles:detail', args=(title_id, )))
    else:
        form = ReviewForm()
    context = {'form': form, 'title': title}
    return render(request, 'titles/review.html', context)

class ListListView(generic.ListView):
    model = List
    template_name = 'titles/lists.html'


class ListCreateView(generic.CreateView):
    model = List
    template_name = 'titles/create_list.html'
    fields = ['name', 'author', 'titles']
    success_url = reverse_lazy('titles:lists')
